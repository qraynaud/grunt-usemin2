var deferred = require('deferred');

var fs = require('fs');
var path = require('path');
var readFile = deferred.promisify(fs.readFile);
var writeFile = deferred.promisify(fs.writeFile);

module.exports = function(grunt) {
  var _ = grunt.util._;

  // Replace __min__ in string/array[string] depending on conf
  function replaceMin(file, release) {
    return (function doit(file) {
      if (_.isString(file)) {
        if (!~file.indexOf('__min__'))
          return file;
        file = file.replace('__min__', release ? '.min' : '');
        if (!release)
          file = [file, '!*.min.*'];
        return file;
      }
      return _.map(file, doit);
    })(file);
  }

  // Transform a file object array as given to usemin2 to an array of
  // real files. Install te files when needed (if mode isn't release).
  function filesObjToArray(obj, type, release) {
    // For each file obj in array, map it to an array of file
    // then fatten the resulting array of arrays
    return _.flatten(_.map(obj, function(file) {
      // Normalize file to an object containing an array
      if (!_.isObject(file)) {
        file = {
          src: file
        }
      };

      // Expand the pattern configured for current mode
      // to an actual list of files
      var files = grunt.file.expand(file, replaceMin(file.src, release));

      if (release) {
        // Always expend the pattern confed in non release mode
        var filesNoMin = grunt.file.expand(file, replaceMin(file.src, false));
        // Look for non minified versions of files for which
        // we didn't found a minified version & add those into
        // the resulting array of files
        _.forEach(filesNoMin, function(f) {
          if (files.indexOf(f) >= 0)
            return;
          if (files.indexOf(f.replace('.' + type, '.min.' + type)) < 0)
            files.push(f);
        });
      }

      // Map each files to the file source that will be needed for processing
      return _.map(files, function(efile) {
        // Append cwd to srcFile if needed
        var srcFile = path.join(file.cwd, efile);

        // If we are not in release mode & have a dest
        if (!release && file.dest) {
          // Install the file & return the resulting path instead of the src path
          var dest = path.join(file.dest, efile);
          grunt.file.copy(srcFile, dest);
          return dest;
        }

        // Return the complete path to the file
        return srcFile;
      });
    }));
  }

  // Configure a task & plan it for run
  function configTasks(tasks, subname, files, dest) {
    // If there is nothing to do in this task, skip it
    if (!files.length)
      return;

    // Normalize tasks into an array
    if (!_.isArray(tasks))
      tasks = [tasks];

    _.forEach(tasks, function(task) {
      // Generate configuration
      var config = {
        files: [{
          src: files,
          dest: dest
        }]
      };

      // Next run should only use the unique file returned by previous task
      files = [dest];

      // Save config
      grunt.config(task + '.' + subname, config);
      // Plan run
      grunt.task.run(task + ':' + subname);
    });

  }

  // Returns a function to generate task configuration
  function generateConfFor(type, options, release) {
    // Retrieve conf for the given type
    var conf = grunt.config(usemin2.name + '.' + type);
    // Normalize conf to an object
    if (!_.isObject(conf))
      conf = {};

    // Compute file list for each section for further processing
    var res = {};

    // For each sections in the conf (name => conf)
    _.forOwn(conf, function(section, name) {
      // Convert the file object to an array of files
      var files = filesObjToArray(section.files, type, release);

      // If we are not in release mode, we are done
      if (!release) {
        // Add the file list for the current section
        res[name] = files;
        return;
      }

      // Resulting file is section destination file "only"
      res[name] = [section.dest];

      // Filter non min files
      var nomins = _.filter(files, function(file) {
        return file.indexOf('.min') < 0;
      });
      // Deduce min files
      var mins = _.difference(files, nomins);
      // Add our minified file to the min files list
      mins.unshift(section.dest);

      // Minification configuration
      var minify = {
        files: [{
          src: nomins,
          dest: section.dest
        }]
      };

      // Concatenation configuration
      var concat = {
        files: [{
          src: mins,
          dest: section.dest
        }]
      };

      // Configure the tasks in grunt & run them
      configTasks(
        options[type + 'min'],
        usemin2.name + '_' + type + '_' + name,
        nomins,
        section.dest);
      configTasks(
        options.concat,
        usemin2.name + '_' + type + '_' + name,
        mins,
        section.dest);
    });

    return res;
  }

  var generators = {
    css: function(src) {
      return '<link href="' + src + '" rel="stylesheet" type="text/css" />';
    },
    js: function(src) {
      return '<script src="' + src + '" type="text/javascript"></script>';
    }
  };

  function generateTag(type, src, options) {
    if (('absoluteBaseDir' in options) || ('baseDir' in options))
      src = path.relative(
        options.absoluteBaseDir || options.baseDir,
        src);
    if ('absoluteBaseDir' in options)
      src = '/' + src;
    src = encodeURI(src);
    if (!(type in generators) || !_.isFunction(generators[type]))
      return '<!-- error: no generator for type "' + type + '" -->';
    return generators[type](src);
  }

  var sectionR = /<!--\s*usemin2:(js|css):(\w+)\s*-->/m;
  function replaceSections(html, filesLists, options) {
    var match;
    var res = '';
    while ((match = html.match(sectionR))) {
      res += html.substring(0, match.index);
      html = html.substring(match.index + match[0].length);

      _.forEach(
        (filesLists[match[1]] || {})[match[2]] || [],
        function(file) {
          res += generateTag(match[1], file, options);
        }
      );
    }
    res += html;
    return res;
  }

  // Usemin2 entry point
  function usemin2(mode) {
    // This is an async task
    var done = this.async();

    // Deduce the current mode from arguments
    var release = mode === 'release';

    // Load options
    var options = this.options({
      cssmin: 'cssmin',
      jsmin: 'uglify',
      concat: 'concat'
    });

    // Generate & run each configuration
    // Get section file list in return
    var filesLists = {};
    ['css', 'js'].forEach(function(type) {
      filesLists[type] = generateConfFor(type, options, release);
    });

    // Check we have an html attribute in configuration
    this.requiresConfig(usemin2.name + '.html');
    var html = grunt.config(usemin2.name + '.html');
    // Normalize html to an array
    if (!_.isArray(html))
      html = [html];

    // Expand all files & flatten
    html = _.flatten(html.map(function(file) {
      return grunt.file.expand(file);
    }));

    // For each html file
    deferred(html)
    .map(function(file) {
      // Read it into an utf8 string
      return readFile(file, 'utf-8')
      // Replace sections
      .then(function(html) {
        return replaceSections(html, filesLists, options);
      })
      // Write file back
      .then(function(html) {
        return writeFile(file, html);
      });
    })
    // Finished, call the done version with the correct param
    // depending on success / failure respectively
    .then(function() {
      done();
    }, function() {
      done(false);
    })
    .done();
  }

  // Register usemin2 task
  grunt.registerTask(
    usemin2.name,
    "Generates automatically the configuration for JS & CSS concat/min processes",
    usemin2
  );
};
