# grunt-usemin2
An alternative to grunt-usemin

## Usage

### Configuration

```javascript
{
  options: {
    // If provided, then set all path in html files relative to this directory
    baseDir: "baseDir/",

    // Same thing as baseDir but forces path to be absolute to this directory
    absoluteBaseDir: "baseDir/",

    // Task(s) to execute to process the css
    cssmin: 'cssmin',

    // Task(s) to execute to process the js
    jsmin: 'uglify'
  },

  // This should contain a reference to all HTML files that usemin2
  // needs to process
  html: 'src/*.html',

  // This section contain everything about css files processing
  css: {
    // You can create as much section as you want with
    // any name you want to use
    section_name: {
      // Each section should define a destination that point to the file
      // that will be created if the minification process is executed
      dest: "dest/css/section_name.min.css",

      // Files that needs to be processed for this section
      files: [{
        // Same as usual
        cwd: "src",
        // List of src (can be an array), each can be expanded,
        // you can also use a special "__min__" markup to select
        // thje correct file depending on the running process
        src: ["css/*.css", "externals/css/*__min__.css"]
        // Destination of the files when no minification process
        // occurs
        dest: "dest/"
      } /* , ... */]
    }/* , ... */
  },

  // Same as css but for js files
  js: {
    // ...
  }
}
```

#### Options

* `baseDir`: not set by default, set all path in html files relative to this directory.
* `absoluteBaseDir`: same as `baseDir` but forces path to be absolute from this point
  (starting with a /).
* `cssmin`: defaults to `'cssmin'`, tasks to execute to minify CSS files. Also
  accepts an array of tasks like `['cssmin1', 'cssmin2']`. In this case, it supposes
  that the first task is able to produce a single file from numerous inputs. If it
  does not, use 'concat' first like in: `['concat', 'taskonsinglefile', 'cssmin']`.
* `jsmin`: same as `cssmin` but for js minification process, defaults to `'uglify'`.

### HTML files

Just add simple comments to your HTML file that will be replaced by
the correct include(s) depending on the process :

```html
<!-- usemin2:css:section_name -->
<!-- usemin2:js:section_name -->
```

### Invokation

Right now, by default grunt-usemin2 assume it is in `dev mode`. It means that
it will publish each files listed in src sections to its corresponding
destination and then replace sections in HTML files by a list of all moved files.

To activate the minification process, you need to be in `release mode`. To do so,
you have to pass a `release` argument to the task just like that :

```grunt usemin2:release```

Any argument value other than `release` is ignored thus making the task behave
as if in `dev mode`.

When the minification process is used, each task of the process is executed resulting
in a single file per section. This is the single file that will be included in the
HTML file where the section is referenced.

## License

[MIT License](http://en.wikipedia.org/wiki/MIT_License)
