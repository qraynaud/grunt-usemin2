var usemin2 = module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json')
  });

  usemin2.registerNpmTasks(grunt);
};
usemin2.registerNpmTasks = function(grunt) {
  grunt.loadTasks(__dirname + '/tasks');
};
